//: Playground - noun: a place where people can play

import UIKit


// Type inference
var age = 30

// Explicity declared type
var weight:Int = 200



var someNum: Double = 9099494987534957493754395493

var milesRan:Float = 56.45
var pi : Float = 3.14
pi = milesRan

// Arithmetic operators

// + - * /

var area = 15 * 20

var sum = 10 + 90

var diff = 34-9
var div = 20/3

var div2:Float = 12 / 5

var reminder = 12 % 5



var randomNumber = 13

if randomNumber % 2 == 0 {
    print("this is an even number")
}else{
    print("this number is odd ")
}

var result2 = 15 * ((5 + 6) / 3)





