//: Playground - noun: a place where people can play

import UIKit

class Vehicle {
    var tires = 4
    var make :String?
    var model:String?
    var currentSpeed: Double = 0
    
    
    init() {
        print("I am the parent")
    }
    
    func drives(speedIncreate: Double) {
        currentSpeed += currentSpeed * 2
    }
    
    func brak() {
        //  make
    }
    
}

class SportsCar : Vehicle {
    override init() {
        super.init()
        make = "BMW"
        model = "3 series"
    }
    
    
    
    override func drives(speedIncreate: Double) {
        currentSpeed += currentSpeed * 3
    }
}

var car = SportsCar()
car.model = "Mheo"

car.currentSpeed = 1
car.drives(speedIncreate: 4)

var vehicle = Vehicle()
vehicle.currentSpeed = 1

vehicle.drives(speedIncreate: 4)

print(vehicle.currentSpeed)
print(car.currentSpeed)







