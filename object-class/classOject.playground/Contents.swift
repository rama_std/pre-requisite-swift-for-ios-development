//: Playground - noun: a place where people can play

import UIKit

class Vehicle {
    var tires = 4
    var headlights = 2
    var horsepower = 500
    var model = ""
    
    func drive()  {
        // do some thing
    }
    
    func brake() {
        print("do some thing")
    
    }
}

let bmw = Vehicle()

bmw.model = "328I"

let ford = Vehicle()

ford.model = "F150"


ford.brake()

func paseByReferance(vehicle: Vehicle){
    vehicle.model = "Cheese"
}

paseByReferance(vehicle: bmw)
print(bmw.model)

func paseByValue(number : Int) -> Int {
    return number
}

paseByValue(number: 90)



